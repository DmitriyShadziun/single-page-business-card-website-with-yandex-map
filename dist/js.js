ymaps.ready(init);


    function init(){ 
           
        var  myMap = new ymaps.Map("map", {

            center: [53.8751981890735,27.65671515795469],
            //controls: ['smallMapDefaultSet'],
            zoom: 16
        });

        myMap.controls.remove('geolocationControl');
        myMap.controls.remove('searchControl');
        myMap.controls.remove('trafficControl');
        myMap.controls.remove('rulerControl');

        myMap.behaviors.disable([
            'drag',
            'scrollZoom'
        ]);


     var placemark1 = new ymaps.Placemark([53.87409911159328,27.655200371570782], {
        hintContent: 'ул.Центральная 46',
        balloonContent: 'Заезжаем под шлагбаум и направляемся к строению 46к14'
     }, {
        preset: 'islands#violetIcon'
     });

     var placemark2 = new ymaps.Placemark([53.87663733490944,27.65824938151123], {
        hintContent: 'едем сюда, именно сюда',
        balloonContent: 'подьезжаем к воротам и набираем мне на телефон'
     }, {
        preset: 'islands#violetIcon'
     });

     myMap.geoObjects.add(placemark1).add(placemark2);
    }